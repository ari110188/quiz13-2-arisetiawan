const assert = require('chai').assert;
const { remote } = require('webdriverio');

describe.only('Login Privy & Changes OTP default', () => {
    it.only('Login', async () => {
        const browser = await remote({
            capabilities: {
                browserName: 'chrome'
            }
        });

        await browser.url('https://privy.id');
        await browser.$('.d-none > .fa-nav__link > .fa-nav__text').click();
        await browser.pause(10000);
        const url = await browser.getUrl();
        assert.strictEqual(url, 'https://oauth.privy.id/login');
        await browser.$('#__BVID__4').setValue('Uat004');
        await browser.$('#tag-lg001').click();
        await browser.$('#__BVID__6').setValue('Akuntes4');
        await browser.$('#tag-lg001').click();
        await browser.pause(10000);
        const welcomeMessage = await browser.$('.dashboard-header > .h-600').getText();
        assert.include(welcomeMessage, 'Welcome Back, Supriyanto');
        await browser.$('.sidebar-bottom > .btn').click(); // click button setting
        await browser.pause(15000);
        await browser.$$(':nth-child(2) > .submenu__body > :nth-child(2) > .submenu-item__body')[0].click(); // click menu security
        await browser.pause(5000);
        await browser.$('#v-security-0__BV_toggle_').click(); // click drop down
        await browser.$('#v-security-0 > .dropdown-menu > :nth-child(3) > .dropdown-item').click(); // click OTP send by email
        const notificationTitle = await browser.$('.notification-title').getText();
        const notificationContent = await browser.$('.notification-content').getText();
        assert.include(notificationTitle, 'OTP Method Changed');
        assert.include(notificationContent, 'OTP Method is successfully changed');
        await browser.pause(10000);
        await browser.$('#v-step-3__BV_button_ > .text-right').click(); // click profil
        await browser.pause(10000);
        await browser.$$('.d-flex > [href="#"]')[0].click(); // clik log out
        await browser.pause(10000);
        const loginText = await browser.$('.box__body-header > .text-danger').getText();
        assert.include(loginText, 'Log In');
        await browser.deleteSession();
    });
});

  
